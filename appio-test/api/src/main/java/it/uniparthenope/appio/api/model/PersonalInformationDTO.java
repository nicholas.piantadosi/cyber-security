package it.uniparthenope.appio.api.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class PersonalInformationDTO {
	
	private String name;
	
	private String surname;
	
	private String fiscalCode;
	
	private String email;
	
	private String birthDate;

}
