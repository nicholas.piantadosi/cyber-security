package it.uniparthenope.appio.api.resources;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import io.quarkus.qute.Template;
import io.quarkus.qute.TemplateInstance;
import it.uniparthenope.appio.api.service.HomeService;
import it.uniparthenope.appio.api.utilities.Constants;
import lombok.extern.slf4j.Slf4j;

@Path(Constants.EMPTY)
@Slf4j
public class HomeResource {

	@Inject
	Template home;

	@Inject
	HomeService homeService;

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public TemplateInstance get(@QueryParam(Constants.ACCESSTOKEN) String accessToken) {
		return home.data(Constants.PAGE_CONTENTS, homeService.getContents(accessToken));
	}

}
