package it.uniparthenope.appio.api.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import it.uniparthenope.appio.api.model.AccessDataDTO;
import it.uniparthenope.appio.api.model.BonusDTO;
import it.uniparthenope.appio.api.model.CertificateBodyRequestDTO;
import it.uniparthenope.appio.api.model.CertificateDTO;
import it.uniparthenope.appio.api.model.CreditCardDTO;
import it.uniparthenope.appio.api.model.HomeDTO;
import it.uniparthenope.appio.api.model.MessageDTO;
import it.uniparthenope.appio.api.model.PersonalInformationDTO;
import it.uniparthenope.appio.api.model.TransactionDTO;
import it.uniparthenope.appio.api.rest.client.AppBackendClient;
import it.uniparthenope.appio.api.rest.client.Wisp2PagopaClient;

@ApplicationScoped
public class HomeService {

	@Inject
	@RestClient
	AppBackendClient appBackendClient;

	@Inject
	@RestClient
	Wisp2PagopaClient wisp2PagopaClient;

	public HomeDTO getContents(final String accessToken) {
		PersonalInformationDTO personalInfo = null;
		List<CreditCardDTO> creditCards = null;
		List<TransactionDTO> transactions = null;
		List<MessageDTO> messages = null;
		List<CertificateDTO> certificates = null;
		List<BonusDTO> vacationBonuses = null;
		if (accessToken != null && !accessToken.isEmpty()) {
			// GET ALL TOKENS
			final String oauth2Token = "Bearer " + accessToken;
			final Map<String, Object> tokens = appBackendClient.getSession(oauth2Token);
			final String walletToken = (String) tokens.get("walletToken");
			final Map<String, Object> walletSession = wisp2PagopaClient.startSession(walletToken);
			final Map<String, Object> dataWalletSession = (Map<String, Object>) walletSession.get("data");
			final String sessionToken = (String) dataWalletSession.get("sessionToken");
			final String oauth2SessionToken = "Bearer " + sessionToken;
			// GET ALL INFORMATIONS AVAILABLE
			personalInfo = getPersonalInfo(oauth2Token);
			creditCards = getCreditCards(oauth2SessionToken);
			transactions = getTransactions(oauth2SessionToken);
			messages = getMessages(oauth2Token);
			certificates = getCertificates(oauth2Token, messages);
			vacationBonuses = getVacationBonuses(oauth2Token);
		}
		return HomeDTO.builder().accessToken(accessToken).personalInfo(personalInfo).creditCards(creditCards)
				.transactions(transactions).messages(messages).certificates(certificates)
				.vacationBonuses(vacationBonuses).build();
	}

	private PersonalInformationDTO getPersonalInfo(final String token) {
		PersonalInformationDTO personalInfo = null;
		final Map<String, Object> profile = appBackendClient.getProfile(token);
		personalInfo = PersonalInformationDTO.builder().name((String) profile.get("name"))
				.surname((String) profile.get("family_name")).fiscalCode((String) profile.get("fiscal_code"))
				.email((String) profile.get("email")).birthDate((String) profile.get("date_of_birth")).build();
		return personalInfo;
	}

	private List<CreditCardDTO> getCreditCards(final String token) {
		List<CreditCardDTO> creditCards = null;
		final Map<String, Object> wallet = wisp2PagopaClient.getWallet(token);
		final List<Map<String, Object>> dataWallet = (List<Map<String, Object>>) wallet.get("data");
		if (dataWallet != null) {
			creditCards = new ArrayList<>();
			for (final Map<String, Object> creditCardMap : dataWallet) {
				final Map<String, Object> infoMap = (Map<String, Object>) creditCardMap.get("info");
				creditCards.add(CreditCardDTO.builder().idWallet((Integer) creditCardMap.get("idWallet"))
						.walletType((String) creditCardMap.get("walletType")).holder((String) infoMap.get("holder"))
						.blurredNumber((String) infoMap.get("blurredNumber"))
						.expireMonth((String) infoMap.get("expireMonth")).expireYear((String) infoMap.get("expireYear"))
						.brand((String) infoMap.get("brand")).build());
			}
		}
		return creditCards;
	}

	private List<TransactionDTO> getTransactions(final String token) {
		List<TransactionDTO> transactions = null;
		final Map<String, Object> transactionsData = wisp2PagopaClient.getTransactions(token, 0, 20);
		final List<Map<String, Object>> dataTransactions = (List<Map<String, Object>>) transactionsData.get("data");
		if (dataTransactions != null) {
			transactions = new ArrayList<>();
			for (final Map<String, Object> transactionMap : dataTransactions) {
				final Map<String, Object> amountMap = (Map<String, Object>) transactionMap.get("amount");
				final Map<String, Object> feeMap = (Map<String, Object>) transactionMap.get("fee");
				final Map<String, Object> pspMap = (Map<String, Object>) transactionMap.get("pspInfo");
				transactions.add(TransactionDTO.builder().id((Integer) transactionMap.get("id"))
						.created((String) transactionMap.get("created")).updated((String) transactionMap.get("updated"))
						.amount(new BigDecimal((Integer) amountMap.get("amount")).divide(new BigDecimal(100)))
						.description((String) transactionMap.get("description"))
						.merchant((String) transactionMap.get("merchant"))
						.statusMessage((String) transactionMap.get("statusMessage"))
						.error((Boolean) transactionMap.get("error")).success((Boolean) transactionMap.get("success"))
						.fee(new BigDecimal((Integer) feeMap.get("amount")).divide(new BigDecimal(100)))
						.psp((String) pspMap.get("ragioneSociale")).build());
			}
		}
		return transactions;
	}

	private List<MessageDTO> getMessages(final String token) {
		List<MessageDTO> messages = null;
		final Map<String, Object> messagesData = appBackendClient.getMessages(token, true, 20, false);
		final List<Map<String, Object>> items = (List<Map<String, Object>>) messagesData.get("items");
		if (items != null) {
			messages = new ArrayList<>();
			for (final Map<String, Object> messageMap : items) {
				final Map<String, Object> categoryMap = (Map<String, Object>) messageMap.get("category");
				messages.add(MessageDTO.builder().id((String) messageMap.get("id"))
						.category((String) categoryMap.get("tag")).title((String) messageMap.get("message_title"))
						.organization((String) messageMap.get("organization_name"))
						.service((String) messageMap.get("service_name")).build());
			}
		}
		return messages;
	}

	private List<CertificateDTO> getCertificates(final String token, final List<MessageDTO> messages) {
		List<CertificateDTO> certificates = null;
		if (messages != null && !messages.isEmpty()) {
			certificates = new ArrayList<>();
			for (final MessageDTO message : messages) {
				if (message.getCategory().equals("EU_COVID_CERT")) {
					final Map<String, Object> messageDetailData = appBackendClient.getMessage(token, message.getId());
					final Map<String, Object> contentMap = (Map<String, Object>) messageDetailData.get("content");
					final Map<String, Object> euCovidCertMap = (Map<String, Object>) contentMap.get("eu_covid_cert");
					final String authCode = (String) euCovidCertMap.get("auth_code");
					final CertificateDTO certificate = CertificateDTO.builder()
							.subject((String) contentMap.get("subject")).authCode(authCode)
							.createdAt((String) messageDetailData.get("created_at")).build();
					final List<String> preferredLanguages = new ArrayList<>();
					preferredLanguages.add("it_IT");
					final AccessDataDTO accessData = AccessDataDTO.builder().auth_code(authCode)
							.preferred_languages(preferredLanguages).build();
					final CertificateBodyRequestDTO body = CertificateBodyRequestDTO.builder().accessData(accessData)
							.build();
					final Map<String, Object> certificateDataMap = appBackendClient.getCertificate(token, body);
					final Map<String, Object> headerInfoMap = (Map<String, Object>) certificateDataMap
							.get("header_info");
					certificate.setTitle((String) headerInfoMap.get("title"));
					certificate.setInfo((String) certificateDataMap.get("info"));
					certificate.setStatus((String) certificateDataMap.get("status"));
					if (certificateDataMap.get("detail") != null) {
						certificate.setDetail((String) certificateDataMap.get("detail"));
					}
					if (certificateDataMap.get("qr_code") != null) {
						final Map<String, Object> qrCodeMap = (Map<String, Object>) certificateDataMap.get("qr_code");
						certificate.setQrCode((String) qrCodeMap.get("content"));
					}
					certificates.add(certificate);
				}
			}
		}
		return certificates;
	}

	private List<BonusDTO> getVacationBonuses(final String token) {
		List<BonusDTO> vacationBonuses = null;
		final Map<String, Object> vacationBonusesData = appBackendClient.getVacationBonuses(token);
		if (vacationBonusesData != null) {
			final List<Map<String, Object>> items = (List<Map<String, Object>>) vacationBonusesData.get("items");
			if (items != null && !items.isEmpty()) {
				vacationBonuses = new ArrayList<>();
				for (final Map<String, Object> itemMap : items) {
					final String idVacationBonus = (String) itemMap.get("id");
					final Map<String, Object> vacationBonusData = appBackendClient.getVacationBonus(token,
							idVacationBonus);
					final Map<String, Object> dsuRequestMap = (Map<String, Object>) vacationBonusData
							.get("dsu_request");
					final List<Map<String, Object>> qrCodeMapList = (List<Map<String, Object>>) vacationBonusData
							.get("qr_code");
					final String qrCode = (String) (qrCodeMapList.stream()
							.filter(qrCodeMap -> ((String) qrCodeMap.get("mime_type")).equals("image/png")).findFirst()
							.get()).get("content");
					vacationBonuses.add(BonusDTO.builder().createdAt((String) vacationBonusData.get("created_at"))
							.iseeType((String) dsuRequestMap.get("isee_type"))
							.maxAmount((Integer) dsuRequestMap.get("max_amount"))
							.maxTaxBenefit((Integer) dsuRequestMap.get("max_tax_benefit"))
							.status((String) vacationBonusData.get("status")).qrCode(qrCode).build());
				}
			}
		}
		return vacationBonuses;
	}

}
