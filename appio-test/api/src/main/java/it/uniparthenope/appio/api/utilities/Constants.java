package it.uniparthenope.appio.api.utilities;

public class Constants {
	
	public static final String VERSION = "Version";
	
	public static final String EMPTY = "";
	
	public static final String PAGE_CONTENTS = "pageContents";
	
	public static final String ACCESSTOKEN = "accessToken";

}
