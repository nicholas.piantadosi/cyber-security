package it.uniparthenope.appio.api.rest.client;

import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@ApplicationScoped
@RegisterRestClient(configKey = "wisp2-pagopa-restapi")
public interface Wisp2PagopaClient {

	@GET
	@Path("/v1/users/actions/start-session")
	Map<String, Object> startSession(@QueryParam(value = "token") final String walletToken);

	@GET
	@Path("/v3/wallet")
	Map<String, Object> getWallet(@HeaderParam("Authorization") String sessionToken);

	@GET
	@Path("/v1/transactions")
	Map<String, Object> getTransactions(@HeaderParam("Authorization") String sessionToken,
			@QueryParam(value = "start") final int start, @QueryParam(value = "size") final int size);

}
