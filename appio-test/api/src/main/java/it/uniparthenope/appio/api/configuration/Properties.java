package it.uniparthenope.appio.api.configuration;

import javax.inject.Singleton;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Singleton
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Properties {

	@ConfigProperty(name = "app.info.version")
	private String version;

}
