package it.uniparthenope.appio.api.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class CreditCardDTO {
	
	private Integer idWallet;
	
	private String walletType;
	
	private String holder;
	
	private String brand;
	
	private String blurredNumber;
	
	private String expireMonth;
	
	private String expireYear;

}
