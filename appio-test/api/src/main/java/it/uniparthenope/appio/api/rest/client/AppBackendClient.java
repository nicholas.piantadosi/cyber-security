package it.uniparthenope.appio.api.rest.client;

import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import it.uniparthenope.appio.api.model.CertificateBodyRequestDTO;

@ApplicationScoped
@RegisterRestClient(configKey = "app-backend-io")
public interface AppBackendClient {

	@GET
	@Path("/v1/session")
	Map<String, Object> getSession(@HeaderParam("Authorization") String accessToken);

	@GET
	@Path("/v1/profile")
	Map<String, Object> getProfile(@HeaderParam("Authorization") String accessToken);

	@GET
	@Path("/v1/messages")
	Map<String, Object> getMessages(@HeaderParam("Authorization") String accessToken,
			@QueryParam(value = "enrich_result_data") final boolean enrichResultData,
			@QueryParam(value = "page_size") final int pageSize,
			@QueryParam(value = "archived") final boolean archived);

	@GET
	@Path("/v1/messages/{id}")
	Map<String, Object> getMessage(@HeaderParam("Authorization") String accessToken,
			@PathParam(value = "id") final String id);

	@POST
	@Path("/v1/eucovidcert/certificate")
	Map<String, Object> getCertificate(@HeaderParam("Authorization") String accessToken,
			CertificateBodyRequestDTO body);

	@GET
	@Path("/v1/bonus/vacanze/activations")
	Map<String, Object> getVacationBonuses(@HeaderParam("Authorization") String accessToken);

	@GET
	@Path("/v1/bonus/vacanze/activations/{id}")
	Map<String, Object> getVacationBonus(@HeaderParam("Authorization") String accessToken,
			@PathParam(value = "id") final String id);

}
