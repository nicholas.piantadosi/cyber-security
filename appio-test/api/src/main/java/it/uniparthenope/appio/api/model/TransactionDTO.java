package it.uniparthenope.appio.api.model;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class TransactionDTO {
	
	private Integer id;
	
	private String created;
	
	private String updated;
	
	private BigDecimal amount;
	
	private BigDecimal fee;
	
	private String description;
	
	private String merchant;
	
	private String statusMessage;
	
	private Boolean error;
	
	private Boolean success;
	
	private String psp;

}
