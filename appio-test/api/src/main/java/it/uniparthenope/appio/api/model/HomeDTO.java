package it.uniparthenope.appio.api.model;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class HomeDTO implements Serializable {

	private static final long serialVersionUID = 562805508318359553L;
	
	private String accessToken;
	
	private PersonalInformationDTO personalInfo;
	
	private List<CreditCardDTO> creditCards;
	
	private List<TransactionDTO> transactions;
	
	private List<MessageDTO> messages;
	
	private List<CertificateDTO> certificates;
	
	private List<BonusDTO> vacationBonuses;
	
}
