package it.uniparthenope.appio.api.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class CertificateDTO {
	
	private String subject;
	
	private String authCode;
	
	private String createdAt;
	
	private String title;
	
	private String info;
	
	private String status;
	
	private String detail;
	
	private String qrCode;
	
}
