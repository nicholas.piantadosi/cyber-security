package it.uniparthenope.appio.api.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class BonusDTO {
	
	private String createdAt;
	
	private String iseeType;
	
	private Integer maxAmount;
	
	private Integer maxTaxBenefit;
	
	private String status;
	
	private String qrCode;
	
}
