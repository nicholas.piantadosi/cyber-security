package it.uniparthenope.appio.api.resources;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import io.quarkus.qute.Template;
import io.quarkus.qute.TemplateInstance;
import it.uniparthenope.appio.api.utilities.Constants;

@Path("/requirements")
public class RequirementsResource {

	@Inject
	Template requirements;

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public TemplateInstance get(@QueryParam(Constants.ACCESSTOKEN) String accessToken) {
		return requirements.data(null);
	}

}
