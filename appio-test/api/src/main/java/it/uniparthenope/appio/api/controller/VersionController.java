package it.uniparthenope.appio.api.controller;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import it.uniparthenope.appio.api.configuration.Properties;
import it.uniparthenope.appio.api.utilities.Constants;

@Path("/version")
@ApplicationScoped
public class VersionController {

	@Inject
	Properties properties;

	@GET
	public Response getVersion() {
		final Map<String, String> outMap = new HashMap<>();
		outMap.put(Constants.VERSION, properties.getVersion());
		return Response.ok(outMap).build();
	}
}
