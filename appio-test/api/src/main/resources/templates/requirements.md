<!DOCTYPE html>
<html lang="it">
<head>
<!-- TITLE -->
<title>Cyber Security - Requirements</title>
</head>
<body>
	<h1>Requirements</h1>
	<p>
		All information refer to the slides available <a target="_blank" href="https://docs.google.com/presentation/d/11_on8jwj3fih4jiy7cq0Z-gypW7PsrF5_DVk_QIIKds/edit?usp=sharing">here</a>
	</p>
	<p>
		<h2>Req. 1 - Get the OAuth2 token - Slide 28-32</h2>
		<ul>
			<li>Install Charles Proxy</li>
			<li>Install certificate on device</li>
			<li>Configure manual proxy to Charles Proxy on device</li>
			<li>Start sniffing</li>
			<li>Get "Bearer" token from "Authorization" header</li>
		</ul>
	</p>
	<p>
		<h2>Req. 2 - Get all tokens and start new session - Slide 33-34, 37</h2>
		<ul>
			<li>Load the home page (http://localhost:8080/appio/test/home.html) -> <a href="http://localhost:8080/appio/test/" target="_blank">click here to open the home page on new tab</a></li>
			<li>Insert token inside the input field (Access Token...)</li>
			<li>Click on "load" button</li>
		</ul>
	</p>
	<p>
		<h2>Req. 3 - Personal information - Slide 35-36</h2>
		<ul>
			<li>Personal information are displayed in this section</li>
			<li>Here are displayed: name, surname, fiscal code, email and birth date</li>
		</ul>
	</p>
	<p>
		<h2>Req. 4 - Credit cards from wallet - Slide 38-39</h2>
		<ul>
			<li>Credit cards configured in APP-IO are displayed in this section</li>
			<li>The list contains information about wallet type, holder, blurred number (last 4 digit) and expiration</li>
		</ul>
	</p>
	<p>
		<h2>Req. 5 - Transactions - Slide 40</h2>
		<ul>
			<li>Last 20 transactions of payments executed are displayed in this section</li>
			<li>The table contains information about merchant, creation date, amount and fee of payment, status and PSP used for payment</li>
		</ul>
	</p>
	<p>
		<h2>Other requirements - Messages, certificates, and bonuses</h2>
		<ul>
			<li>Other sections are also displayed</li>
			<ul>
				<li>
					Messages
				</li>
				<ul>
					<li>Last 20 messages received are listed</li>
					<li>A category is listed for every message (if category is "EU_COVID_CERT" the message details are used to recoved covid certifications on next section)</li>
				</ul>
				<li>
					Certificates
				</li>
				<ul>
					<li>All certificates found are listed here</li>
					<li>Information about subject, creation date, title, additional information and status are displayed</li>
					<li>If present, the qr code is shown with the direct link to the green pass</li>
				</ul>
				<li>
					Bonuses
				</li>
				<ul>
					<li>Vacation bonuses are displayed in this section (if presents)</li>
					<li>Information about ISEE type is also reported</li>
				</ul>
			</ul>
		</ul>
	</p>
</body>
</html>