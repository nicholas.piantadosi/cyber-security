# Cyber Security

## appio-test

### PRE REQUISITES

* JDK17
* maven

### HOW TO RUN

cd appio-test/api
mvn clean quarkus:dev

### REQUIREMENTS

* http://localhost:8080/appio/test/requirements

### HOME PAGE

* http://localhost:8080/appio/test/
